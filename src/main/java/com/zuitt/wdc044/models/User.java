package com.zuitt.wdc044.models;
import javax.persistence.*;

@Entity
@Table(name="users")
public class User {
    //User model properties;
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String Username;

    @Column
    private String Password;

    //default constructor
    public User(){}

    //parameterized constructor
    public User(String Username, String Password){

        this.Username = Username;
        this.Password = Password;

    }
    //Getters and setters for username and password
    public String getUsername() {return Username;}
    public void setUsername(String Username){this.Username = Username;}

    public String getPassword() {return Password;}
    public void setPassword(String Password){this.Password = Password;}

    //Getter for id
    public Long getId() {return id;}


}
